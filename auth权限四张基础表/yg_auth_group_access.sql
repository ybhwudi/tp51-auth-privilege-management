/*
 Navicat Premium Data Transfer

 Source Server         : wudi
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : yg_wudi

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 14/07/2021 17:46:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yg_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `yg_auth_group_access`;
CREATE TABLE `yg_auth_group_access`  (
  `uid` mediumint(8) UNSIGNED NOT NULL COMMENT '用户id',
  `group_id` mediumint(8) UNSIGNED NOT NULL COMMENT '角色id',
  UNIQUE INDEX `uid_group_id`(`uid`, `group_id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `group_id`(`group_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色分配表' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of yg_auth_group_access
-- ----------------------------
INSERT INTO `yg_auth_group_access` VALUES (1, 1);
INSERT INTO `yg_auth_group_access` VALUES (2, 2);

SET FOREIGN_KEY_CHECKS = 1;
