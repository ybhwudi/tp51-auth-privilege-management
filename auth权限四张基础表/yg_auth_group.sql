/*
 Navicat Premium Data Transfer

 Source Server         : wudi
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : yg_wudi

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 14/07/2021 17:46:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yg_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `yg_auth_group`;
CREATE TABLE `yg_auth_group`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '角色中文名称',
  `remarks` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色备注',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态：为1正常，为0禁用',
  `rules` char(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '角色拥有的规则id， 多个规则\",\"隔开',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yg_auth_group
-- ----------------------------
INSERT INTO `yg_auth_group` VALUES (1, '超级管理员', '拥有至高无上的权限', 1, '*');
INSERT INTO `yg_auth_group` VALUES (2, '员工', '员工', 1, '1,2');

SET FOREIGN_KEY_CHECKS = 1;
