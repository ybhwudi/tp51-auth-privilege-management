/*
 Navicat Premium Data Transfer

 Source Server         : wudi
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : yg_wudi

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 14/07/2021 17:45:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yg_auth_admin
-- ----------------------------
DROP TABLE IF EXISTS `yg_auth_admin`;
CREATE TABLE `yg_auth_admin`  (
  `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `toux` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `admin_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员昵称',
  `sex` tinyint(1) NOT NULL COMMENT '性别   1男 2女',
  `number` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '账号状态 1正常  0封禁',
  `add_time` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '增加时间',
  `up_time` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`admin_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yg_auth_admin
-- ----------------------------
INSERT INTO `yg_auth_admin` VALUES (1, NULL, 'YG-wudi', 1, 'admin', '14e1b600b1fd579f47433b88e8d85291', 1, '123456', '123456');
INSERT INTO `yg_auth_admin` VALUES (2, NULL, 'cs', 1, '123456', '14e1b600b1fd579f47433b88e8d85291', 1, '123456', '123456');

SET FOREIGN_KEY_CHECKS = 1;
